﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Henn;

namespace TestComplex
{
    class Program
    {
        static void Main(string[] args)
        {
            string template = "{0:F2}";
            Console.WriteLine(template, new Complex(7, 4));
            Console.WriteLine(template, new Complex(7, -4));
            Console.WriteLine(template, new Complex(-7, -4));
            Console.WriteLine(template, new Complex(7, 0));

            Console.WriteLine(template, 7.0);

            Complex c = 7.0;
            Console.WriteLine(c);

            Complex i = new Complex(0, 1);
            Console.WriteLine(i*i);

            Complex d = 2;
            Console.WriteLine(2 / i);
            Console.WriteLine((2/i)*i);

        }
    }
}
