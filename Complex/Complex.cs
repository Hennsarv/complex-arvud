﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Henn
{
    public struct Complex : IFormattable
    // viimane vajalik, et saaks ToStringile parameetreid ette anda
    {
        double R;
        double I;

        public Complex(double r = 0.0, double i = 0.0) { R = r; I = i; }

        public static Complex operator+ (Complex x, Complex y)
            {
            return new Complex(x.R + y.R, x.I + y.I);
            }

        public static Complex operator -(Complex x)
        {
            return new Complex(-x.R, -x.I);
        }
        public static Complex operator +(Complex x)
        {
            return x;
        }

        public static Complex operator- (Complex x, Complex y)
        {
            return x + (-y);
        }

        public static Complex operator* (Complex x, Complex y)
        {
            return new Complex(x.R * y.R - x.I * y.I, x.R * y.I + x.I * y.R);
        }

        public static Complex operator *(Complex x, double y)
        {
            return new Complex(x.R * y, x.I * y);
        }
        public static Complex operator *(double y, Complex x)
        {
            return new Complex(x.R * y, x.I * y);
        }
        public static Complex operator/ (Complex x, double y)
        {
            return new Complex(x.R / y, x.I / y);
        }
 
        public static Complex operator/ (Complex x, Complex y)
        {
            double d = y.R * y.R + y.I * y.I;
            return new Complex(x.R * y.R - x.I * y.I, x.I * y.R - x.R * y.I) / d;
        }

        public static bool operator== (Complex x, Complex y)
        {
            return x.R == y.R && x.I == y.I;
        }

        public static bool operator!= (Complex x, Complex y)
        {
            return !(x == y);
        }

        public override bool Equals(object obj)
        {
            if (obj is System.ValueType) return this == new Complex((double)obj);
            else
            return obj is Complex ? this == (Complex)obj : false;
        }

        public override int GetHashCode()
        {
            return R.GetHashCode() + I.GetHashCode();
        }


        public static implicit operator Complex(double x)
        {
            return new Complex(x);
        }



        #region ToStringid
        public override string ToString()
        {
            return (R == 0 ? "" : R.ToString()) + (I == 0 ? "" : (I > 0 ? "+" : "") + I.ToString() + "I");
        }

        string ToString(string format)
        {
            return (R == 0 ? "" : R.ToString(format)) + (I == 0 ? "" : (I > 0 ? "+" : "") + I.ToString(format) + "I");
        }

        string IFormattable.ToString(string format, IFormatProvider formatProvider)
        {
            return (R == 0 ? "" : R.ToString(format, formatProvider)) + (I == 0 ? "" : (I > 0 ? "+" : "") + I.ToString(format, formatProvider) + "I");
        }

        #endregion


    }




    }
